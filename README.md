# GitLab IIS K8s Deploy

## Build an ASP.NET Core image

You can build and run a .NET-based container image using the following instructions:

```console
docker build -t aspnetapp .
docker run --rm -it -p 8000:80 aspnetapp
```

---

## Setup GCP

### 1. Setup GCP Project ID

  ```bash
  gcloud config set project <gcp_project_id>
  ```

### 2. Setup ENV variables

  ```bash
  GCP_PROJECT_ID=<gcp_project_id>
  GCP_CLUSTER_NAME=<gcp_cluster_name>
  ```

### 3. Enable compute, container service

  ```bash
  gcloud services enable compute.googleapis.com
  gcloud services enable container.googleapis.com
  ```

### 4. Create service accounts key

4.1 在 project 的 IAM / Admin > Service accounts > Create service account， 將 Role 設定為:

  * Kubernetes Engine > Kubernetes Engine Developer
  * Cloud Storage > Storage Admin

4.2 建立完 service account 後，建立 Key(JSON)

  ```bash
gcloud iam service-accounts create gitlab-ci \
  --description="gitlab ci" --display-name="gitlab-ci"

gcloud projects add-iam-policy-binding ${GCP_PROJECT_ID} \
    --member="serviceAccount:gitlab-ci@${GCP_PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/storage.admin"

gcloud projects add-iam-policy-binding ${GCP_PROJECT_ID} \
    --member="serviceAccount:gitlab-ci@${GCP_PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/container.developer"

gcloud iam service-accounts keys create ~/gitlab-ci.json \
    --iam-account gitlab-ci@${GCP_PROJECT_ID}.iam.gserviceaccount.com
  ```


## GKE Cluster

### 1.1 Create Cluster

  ```bash
  gcloud container clusters create ${GCP_CLUSTER_NAME} \
    --zone asia-east1-b \
    --num-nodes=1
  ```

### 1.2 Connect GKE with kubectl

  ```
  gcloud container clusters get-credentials ${GCP_CLUSTER_NAME} \
    --zone asia-east1-b \
    --project ${GCP_PROJECT_ID}
  ```

### 1.3 Edit GCR image url

Deployment > spec > template > spec > containers > image

  ```
  image: asia.gcr.io/<gcp_project_id>/<image_name>:latest
  ```

  ![alt text](images/deployment.png "deployment")

### 1.4 Setup K8s service and deployment

  ```
  kubectl apply -f deployment.yaml
  ```

  ![alt text](images/kubectl_get_all.png "kubectl_get_all")

## Setup GitLab CI

### 1. Setting CI/CD Variables

  ```
  GCP_SERVICE_KEY
  GCP_PROJECT
  GCP_ZONE
  GCP_CLUSTER_NAME
  GCP_GCR
  ```

  ![alt text](images/gitlab-ci-setup.png "gitlab-ci-setup")

## Clean up

```
gcloud container clusters delete ${GCP_CLUSTER_NAME}
```


Ref:
  - [ASP.NET Core Docker Sample](https://github.com/dotnet/dotnet-docker/tree/master/samples/aspnetapp)
